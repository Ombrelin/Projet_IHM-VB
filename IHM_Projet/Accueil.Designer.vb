﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Accueil
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Accueil))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ComboBoxNomJoueur = New System.Windows.Forms.ComboBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.BtnJouer = New System.Windows.Forms.Button()
        Me.BtnQuitter = New System.Windows.Forms.Button()
        Me.BtnScore = New System.Windows.Forms.Button()
        Me.BtnCreation = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(338, 130)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(561, 149)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'ComboBoxNomJoueur
        '
        Me.ComboBoxNomJoueur.FormattingEnabled = True
        Me.ComboBoxNomJoueur.Location = New System.Drawing.Point(505, 324)
        Me.ComboBoxNomJoueur.Name = "ComboBoxNomJoueur"
        Me.ComboBoxNomJoueur.Size = New System.Drawing.Size(253, 21)
        Me.ComboBoxNomJoueur.TabIndex = 1
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(578, 284)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(106, 34)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 2
        Me.PictureBox2.TabStop = False
        '
        'BtnJouer
        '
        Me.BtnJouer.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnJouer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnJouer.Font = New System.Drawing.Font("Moyko", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnJouer.ForeColor = System.Drawing.Color.Black
        Me.BtnJouer.Location = New System.Drawing.Point(505, 351)
        Me.BtnJouer.Name = "BtnJouer"
        Me.BtnJouer.Size = New System.Drawing.Size(253, 59)
        Me.BtnJouer.TabIndex = 3
        Me.BtnJouer.Text = " Jouer"
        Me.BtnJouer.UseVisualStyleBackColor = False
        '
        'BtnQuitter
        '
        Me.BtnQuitter.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnQuitter.Font = New System.Drawing.Font("Moyko", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnQuitter.ForeColor = System.Drawing.Color.Black
        Me.BtnQuitter.Location = New System.Drawing.Point(505, 548)
        Me.BtnQuitter.Name = "BtnQuitter"
        Me.BtnQuitter.Size = New System.Drawing.Size(253, 59)
        Me.BtnQuitter.TabIndex = 4
        Me.BtnQuitter.Text = "Quitter"
        Me.BtnQuitter.UseVisualStyleBackColor = False
        '
        'BtnScore
        '
        Me.BtnScore.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnScore.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnScore.Font = New System.Drawing.Font("Moyko", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnScore.ForeColor = System.Drawing.Color.Black
        Me.BtnScore.Location = New System.Drawing.Point(505, 483)
        Me.BtnScore.Name = "BtnScore"
        Me.BtnScore.Size = New System.Drawing.Size(253, 59)
        Me.BtnScore.TabIndex = 4
        Me.BtnScore.Text = " Score"
        Me.BtnScore.UseVisualStyleBackColor = False
        '
        'BtnCreation
        '
        Me.BtnCreation.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnCreation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCreation.Font = New System.Drawing.Font("Moyko", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCreation.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.BtnCreation.Location = New System.Drawing.Point(505, 416)
        Me.BtnCreation.Name = "BtnCreation"
        Me.BtnCreation.Size = New System.Drawing.Size(251, 59)
        Me.BtnCreation.TabIndex = 5
        Me.BtnCreation.Text = "Grille"
        Me.BtnCreation.UseVisualStyleBackColor = False
        '
        'Accueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Khaki
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1201, 624)
        Me.Controls.Add(Me.BtnCreation)
        Me.Controls.Add(Me.BtnScore)
        Me.Controls.Add(Me.BtnQuitter)
        Me.Controls.Add(Me.BtnJouer)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.ComboBoxNomJoueur)
        Me.Controls.Add(Me.PictureBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Accueil"
        Me.Text = "Sudoku"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ComboBoxNomJoueur As ComboBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents BtnJouer As Button
    Friend WithEvents BtnQuitter As Button
    Friend WithEvents BtnScore As Button
    Friend WithEvents BtnCreation As Button
End Class
