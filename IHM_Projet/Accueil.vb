﻿Public Class Accueil

    'Initialisation du formulaire
    Private Sub Accueil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModuleInterface.Ambiance()
        ModuleInterface.ForbidResize(Me)
        For i As Integer = 0 To ModuleJoueur.getNbJoueurs - 1
            ComboBoxNomJoueur.Items.Add(Trim(ModuleJoueur.getNomJoueur(i)))
        Next
    End Sub


    'Lance une partie grace au formulaire jeu
    Private Sub BtnJouer_Click(sender As Object, e As EventArgs) Handles BtnJouer.Click
        If ComboBoxNomJoueur.Text = "" Then
            MsgBox("Veuillez renseigner un nom", MsgBoxStyle.OkOnly)
        Else
            If ModuleJoueur.isNouveau(ComboBoxNomJoueur.Text) Then
                ajoutJoueur(ComboBoxNomJoueur.Text)
                Me.Hide()
                Jeu.Show()
            Else
                Me.Hide()
                Jeu.Show()
            End If
        End If
    End Sub

    'Quitte l'application
    Private Sub BtnQuitter_Click(sender As Object, e As EventArgs) Handles BtnQuitter.Click
        Application.Exit()
    End Sub

    'Ouvre la fenêtre des statistiques
    Private Sub BtnScore_Click(sender As Object, e As EventArgs) Handles BtnScore.Click
        Statistique.Show()
        Me.Hide()
    End Sub

    'Ouvre la fenêtre de création
    Private Sub BtnCreation_Click(sender As Object, e As EventArgs) Handles BtnCreation.Click
        Me.Hide()
        Creation.Show()
    End Sub

    'Effet de swapcolor au survole
    Private Sub Btn_In(sender As Button, e As EventArgs) Handles BtnCreation.MouseEnter, BtnJouer.MouseEnter, BtnQuitter.MouseEnter, BtnScore.MouseEnter
        ModuleInterface.swapColorIn(sender)
    End Sub

    'Effet de swapcolor au survole
    Private Sub Btn_Ouy(sender As Button, e As EventArgs) Handles BtnCreation.MouseLeave, BtnJouer.MouseLeave, BtnQuitter.MouseLeave, BtnScore.MouseLeave
        ModuleInterface.swapColorOut(sender)
    End Sub
End Class
