﻿Imports System.IO
Public Class Creation
    Const NB_CASES_GRID As Integer = 81
    Const NB_CASES_LC_GRID As Integer = 9
    Dim boxes(NB_CASES_GRID - 1) As TextBox

    'Génération des textbox
    Private Sub Creation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModuleInterface.ForbidResize(Me)
        Dim x As Integer = 500
        Dim y As Integer = 250
        Dim k As Integer = 0
        For i As Integer = 1 To NB_CASES_LC_GRID
            For j As Integer = 1 To NB_CASES_LC_GRID
                Dim t As New TextBox
                t.Size = New Size(20, 20)
                t.Location = New Point(x, y)
                If Not (j Mod 3 = 0) Then
                    x += 20
                Else
                    x += 40
                End If
                boxes(k) = t
                AddHandler t.KeyPress, AddressOf TxtBx_GridKeypress
                Me.Controls.Add(t)

                k += 1
            Next
            If Not (i Mod 3 = 0) Then
                y += 20
            Else
                y += 40
            End If

            x = 500
        Next
    End Sub

    'Empêche d'écrire autre chose qu'un chiffre
    Private Sub TxtBx_GridKeypress(sender As TextBox, e As KeyPressEventArgs)
        If ((Not IsNumeric(e.KeyChar)) Or (Not sender.Text.Length = 0)) And Not e.KeyChar = ControlChars.Back Then
            e.Handled = True
        End If
    End Sub

    'Sauvegarde la grille dans un fichier texte, lisible et jouable dans l'application
    Private Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click
        Dim s As String = ""
        For Each t As TextBox In boxes
            If t.Text <> "" Then
                s += t.Text
                s += vbCrLf
            Else
                s += "0"
                s += vbCrLf
            End If
        Next
        Dim output As StreamWriter
        If SaveFileDialog.ShowDialog() = DialogResult.OK Then
            output = My.Computer.FileSystem.OpenTextFileWriter(SaveFileDialog.FileName, True)
            output.WriteLine(s)
            output.Close()
        End If
    End Sub

    'Retour à l'accueil
    Private Sub BtnRetour_Click(sender As Object, e As EventArgs) Handles BtnRetour.Click
        Me.Close()
        Accueil.Show()
    End Sub
End Class