﻿Imports System.IO
Public Class Jeu
    'Constantes de jeu
    Dim temps As Integer = 60
    Const NB_CASES_GRID As Integer = 81
    Const NB_CASES_LC_GRID As Integer = 9
    'Tableaux de stockage
    Dim boxes(NB_CASES_GRID - 1) As TextBox
    Dim carres(NB_CASES_LC_GRID - 1) As Carre
    Dim lignes(NB_CASES_LC_GRID - 1) As Ligne
    Dim colonnes(NB_CASES_LC_GRID - 1) As Colonne

    'Structures de stockage
    Public Structure Carre
        Dim boxes() As TextBox
    End Structure

    Public Structure Ligne
        Dim boxes() As TextBox
    End Structure

    Public Structure Colonne
        Dim boxes() As TextBox
    End Structure

    'Initialisation des structures de stockage du jeu
    Public Sub initBoxes()
        Dim x As Integer = 500
        Dim y As Integer = 250
        Dim k As Integer = 0
        'Loop de création des textBoxes
        For i As Integer = 1 To NB_CASES_LC_GRID
            For j As Integer = 1 To NB_CASES_LC_GRID
                Dim t As New TextBox
                t.Size = New Size(20, 20)
                t.Location = New Point(x, y)
                If Not (j Mod 3 = 0) Then
                    x += 20
                Else
                    x += 40
                End If
                boxes(k) = t
                AddHandler t.TextChanged, AddressOf TxtBx_Grid_TxtChanged
                AddHandler t.KeyPress, AddressOf TxtBx_GridKeypress
                Me.Controls.Add(t)

                k += 1
            Next
            If Not (i Mod 3 = 0) Then
                y += 20
            Else
                y += 40
            End If

            x = 500
        Next
        'Initialisation des carrés
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            Dim c As Carre
            ReDim c.boxes(NB_CASES_LC_GRID - 1)
            carres(i) = c
        Next
        'Initialisation des lignes
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            Dim l As Ligne
            ReDim l.boxes(NB_CASES_LC_GRID - 1)
            lignes(i) = l
        Next
        'Initialisation des des colonnes
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            Dim col As Colonne
            ReDim col.boxes(NB_CASES_LC_GRID - 1)
            colonnes(i) = col
        Next

        'Remplissage des carrés
        carres(0).boxes(0) = boxes(0)
        carres(0).boxes(1) = boxes(1)
        carres(0).boxes(2) = boxes(2)
        carres(0).boxes(3) = boxes(9)
        carres(0).boxes(4) = boxes(10)
        carres(0).boxes(5) = boxes(11)
        carres(0).boxes(6) = boxes(18)
        carres(0).boxes(7) = boxes(19)
        carres(0).boxes(8) = boxes(20)

        carres(1).boxes(0) = boxes(3)
        carres(1).boxes(1) = boxes(4)
        carres(1).boxes(2) = boxes(5)
        carres(1).boxes(3) = boxes(12)
        carres(1).boxes(4) = boxes(13)
        carres(1).boxes(5) = boxes(14)
        carres(1).boxes(6) = boxes(21)
        carres(1).boxes(7) = boxes(22)
        carres(1).boxes(8) = boxes(23)

        carres(2).boxes(0) = boxes(6)
        carres(2).boxes(1) = boxes(7)
        carres(2).boxes(2) = boxes(8)
        carres(2).boxes(3) = boxes(15)
        carres(2).boxes(4) = boxes(16)
        carres(2).boxes(5) = boxes(17)
        carres(2).boxes(6) = boxes(24)
        carres(2).boxes(7) = boxes(25)
        carres(2).boxes(8) = boxes(26)

        carres(3).boxes(0) = boxes(27)
        carres(3).boxes(1) = boxes(28)
        carres(3).boxes(2) = boxes(29)
        carres(3).boxes(3) = boxes(36)
        carres(3).boxes(4) = boxes(37)
        carres(3).boxes(5) = boxes(38)
        carres(3).boxes(6) = boxes(45)
        carres(3).boxes(7) = boxes(46)
        carres(3).boxes(8) = boxes(47)

        carres(4).boxes(0) = boxes(30)
        carres(4).boxes(1) = boxes(31)
        carres(4).boxes(2) = boxes(32)
        carres(4).boxes(3) = boxes(39)
        carres(4).boxes(4) = boxes(40)
        carres(4).boxes(5) = boxes(41)
        carres(4).boxes(6) = boxes(48)
        carres(4).boxes(7) = boxes(49)
        carres(4).boxes(8) = boxes(50)

        carres(5).boxes(0) = boxes(33)
        carres(5).boxes(1) = boxes(34)
        carres(5).boxes(2) = boxes(35)
        carres(5).boxes(3) = boxes(42)
        carres(5).boxes(4) = boxes(43)
        carres(5).boxes(5) = boxes(44)
        carres(5).boxes(6) = boxes(51)
        carres(5).boxes(7) = boxes(52)
        carres(5).boxes(8) = boxes(53)

        carres(6).boxes(0) = boxes(54)
        carres(6).boxes(1) = boxes(55)
        carres(6).boxes(2) = boxes(56)
        carres(6).boxes(3) = boxes(63)
        carres(6).boxes(4) = boxes(64)
        carres(6).boxes(5) = boxes(65)
        carres(6).boxes(6) = boxes(72)
        carres(6).boxes(7) = boxes(73)
        carres(6).boxes(8) = boxes(74)

        carres(7).boxes(0) = boxes(57)
        carres(7).boxes(1) = boxes(58)
        carres(7).boxes(2) = boxes(59)
        carres(7).boxes(3) = boxes(66)
        carres(7).boxes(4) = boxes(67)
        carres(7).boxes(5) = boxes(68)
        carres(7).boxes(6) = boxes(75)
        carres(7).boxes(7) = boxes(76)
        carres(7).boxes(8) = boxes(77)

        carres(8).boxes(0) = boxes(60)
        carres(8).boxes(1) = boxes(61)
        carres(8).boxes(2) = boxes(62)
        carres(8).boxes(3) = boxes(69)
        carres(8).boxes(4) = boxes(70)
        carres(8).boxes(5) = boxes(71)
        carres(8).boxes(6) = boxes(78)
        carres(8).boxes(7) = boxes(79)
        carres(8).boxes(8) = boxes(80)

        'Remplissage des lignes
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            For j As Integer = 0 To NB_CASES_LC_GRID - 1
                lignes(i).boxes(j) = boxes(j + (9 * i))
            Next
        Next

        'Remplissage des colonnes
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            For j As Integer = 0 To NB_CASES_LC_GRID - 1
                colonnes(i).boxes(j) = boxes(i + 9 * j)
            Next
        Next
    End Sub

    'check de la validité des modifs
    Private Function Checkboxes(box As TextBox) As Boolean

        Dim currentCarre As Integer
        Dim currentLigne As Integer
        Dim currentCol As Integer
        'Identifie le carré dans lequel le sender se trouve
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            For j As Integer = 0 To NB_CASES_LC_GRID - 1
                If (carres(i).boxes(j).Equals(box)) Then
                    currentCarre = i
                End If
            Next
        Next

        'Identifie la ligne dans laquel on se trouve
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            For j As Integer = 0 To NB_CASES_LC_GRID - 1
                If (lignes(i).boxes(j).Equals(box)) Then
                    currentLigne = i
                End If
            Next
        Next

        'Identifie la colonne dans la laquelle on se trouve
        For i As Integer = 0 To NB_CASES_LC_GRID - 1
            For j As Integer = 0 To NB_CASES_LC_GRID - 1
                If (colonnes(i).boxes(j).Equals(box)) Then
                    currentCol = i
                End If
            Next
        Next

        'On vérérifie si une autre textBox que le sender a la même valeur dans le carré
        For Each t As TextBox In carres(currentCarre).boxes
            If (t.Text = box.Text) And Not t.Equals(box) Then
                Return False
            End If
        Next

        'On vérérifie si une autre textBox que le sender a la même valeur dans la ligne
        For Each t As TextBox In lignes(currentLigne).boxes
            If (t.Text = box.Text) And Not t.Equals(box) Then
                Return False
            End If
        Next

        'On vérérifie si une autre textBox que le sender a la même valeur dans la colonne
        For Each t As TextBox In colonnes(currentCol).boxes
            If (t.Text = box.Text) And Not t.Equals(box) Then
                Return False
            End If
        Next

        'On vérifie si la configuration actuelle est gagnante
        Dim victoire As Boolean = True
        For Each t As TextBox In boxes
            If t.Text = "" OrElse t.BackColor = Color.Red Then
                victoire = False
            End If
        Next
        If victoire Then
            EndVictoire()
        End If

        'Si on est pas sortie jusque là c'est que la valeur est bonne
        Return True

    End Function

    'Initialisation du plateau
    Private Sub Jeu_Shown(sender As Object, e As EventArgs) Handles MyBase.Activated

    End Sub

    'Handler KeyPress textboxes
    Private Sub TxtBx_GridKeypress(sender As TextBox, e As KeyPressEventArgs)
        If ((Not IsNumeric(e.KeyChar)) OrElse (Not sender.Text.Length = 0)) AndAlso Not e.KeyChar = ControlChars.Back Then
            e.Handled = True
        End If
    End Sub

    Private Sub TxtBx_Grid_TxtChanged(sender As TextBox, e As EventArgs)
        'modification
        Dim isValide As Boolean = Checkboxes(sender)
        If (isValide) Then
            sender.BackColor = SystemColors.Control
        Else
            sender.BackColor = Color.Red
        End If
    End Sub

    'Charge une grille depuis un fichier
    Sub readGridContent()
        Dim i As Integer
        Dim fileName As String
        If FileDialog.ShowDialog() = DialogResult.OK Then
            fileName = FileDialog.FileName
        Else
            fileName = "grid.txt"
        End If
        Dim f As StreamReader = New StreamReader(fileName)
        For Each t As TextBox In boxes
            i = f.ReadLine
            If Not i = 0 Then
                t.Text = i
                t.Enabled = False
            End If
        Next
    End Sub

    'Fin à l'écoulement du temps
    Private Sub Endgame()
        Dim currentJoueur As Integer
        For i As Integer = 0 To ModuleJoueur.getNbJoueurs() - 1
            If Accueil.ComboBoxNomJoueur.Text = ModuleJoueur.getJoueur(i).Nom Then
                currentJoueur = i
            End If
        Next
        ModuleJoueur.majStatJoueur(currentJoueur, 60, 60)
        Me.Close()
        Accueil.Show()
    End Sub

    'Fin en cas de victoire
    Private Sub EndVictoire()
        Dim currentJoueur As Integer
        For i As Integer = 0 To ModuleJoueur.getNbJoueurs() - 1
            If Accueil.ComboBoxNomJoueur.Text = ModuleJoueur.getNomJoueur(i) Then
                currentJoueur = i
            End If
        Next
        ModuleJoueur.majStatJoueur(currentJoueur, 60 - LabelTimer.Text, 60 - LabelTimer.Text)
        MsgBox("Victoire !", MsgBoxStyle.OkOnly)
        Me.Close()
        Accueil.Show()
    End Sub

    'Fin quand on click sur retour
    Private Sub EndRetour()
        Dim currentJoueur As Integer

        For i As Integer = 0 To ModuleJoueur.getNbJoueurs() - 1
            If Accueil.ComboBoxNomJoueur.Text = ModuleJoueur.getNomJoueur(i) Then
                currentJoueur = i
            End If
        Next
        ModuleJoueur.majStatJoueur(currentJoueur, 60, 60 - LabelTimer.Text)
    End Sub

    'Bouton retour menu principal
    Private Sub BtnRetour_Click(sender As Object, e As EventArgs) Handles BtnRetour.Click
        EndRetour()
        Me.Close()
        Accueil.Show()
    End Sub

    'Tick du Timer
    Private Sub Timer_Tick(sender As Object, e As EventArgs) Handles Timer.Tick
        temps -= 1
        If temps = 0 Then
            Timer.Stop()
            LabelTimer.Text = 0
            MsgBox("Perdu !", MsgBoxStyle.OkOnly)
            Endgame()
            Me.Hide()
            Accueil.Show()
        Else
            LabelTimer.Text = temps
        End If
    End Sub

    'Load du jeu qui appelle toutes les sub pour chaque étape de la préparation
    Private Sub Jeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModuleInterface.ForbidResize(Me)
        NomJoueur.Text = Accueil.ComboBoxNomJoueur.Text

        temps = 60
        LabelTimer.Text = temps

        initBoxes()
        readGridContent()
        Timer.Start()
    End Sub
End Class