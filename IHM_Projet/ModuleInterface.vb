﻿Module ModuleInterface
    'Sub qui lance la musique
    Sub Ambiance()
        My.Computer.Audio.Play("ambiance.wav", AudioPlayMode.BackgroundLoop)
    End Sub

    'Empêche le redimensionnement du formulaire en paramètre
    Sub ForbidResize(f As Form)
        f.FormBorderStyle = FormBorderStyle.Fixed3D
        f.MaximizeBox = False
    End Sub

    'Change la couleur du bouton en param en Aquamarine
    Public Sub swapColorIn(b As Button)
        b.BackColor = Color.Aquamarine
    End Sub

    'Change la couleur du bouton en param en medium sea green
    Public Sub swapColorOut(b As Button)
        b.BackColor = Color.MediumSeaGreen
    End Sub

End Module
