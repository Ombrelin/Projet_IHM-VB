﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Statistique
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Statistique))
        Me.ListBoxJoueurs = New System.Windows.Forms.ListBox()
        Me.ListBoxTemps = New System.Windows.Forms.ListBox()
        Me.ComboBoxNomJoueur = New System.Windows.Forms.ComboBox()
        Me.BtnInfo = New System.Windows.Forms.Button()
        Me.BtnRetour = New System.Windows.Forms.Button()
        Me.BtnAlpha = New System.Windows.Forms.Button()
        Me.BtnTps = New System.Windows.Forms.Button()
        Me.LabelTitre = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ListBoxJoueurs
        '
        Me.ListBoxJoueurs.FormattingEnabled = True
        Me.ListBoxJoueurs.Location = New System.Drawing.Point(12, 212)
        Me.ListBoxJoueurs.Name = "ListBoxJoueurs"
        Me.ListBoxJoueurs.Size = New System.Drawing.Size(395, 498)
        Me.ListBoxJoueurs.TabIndex = 0
        '
        'ListBoxTemps
        '
        Me.ListBoxTemps.FormattingEnabled = True
        Me.ListBoxTemps.Location = New System.Drawing.Point(785, 212)
        Me.ListBoxTemps.Name = "ListBoxTemps"
        Me.ListBoxTemps.Size = New System.Drawing.Size(395, 498)
        Me.ListBoxTemps.TabIndex = 1
        '
        'ComboBoxNomJoueur
        '
        Me.ComboBoxNomJoueur.FormattingEnabled = True
        Me.ComboBoxNomJoueur.Location = New System.Drawing.Point(401, 125)
        Me.ComboBoxNomJoueur.Name = "ComboBoxNomJoueur"
        Me.ComboBoxNomJoueur.Size = New System.Drawing.Size(395, 21)
        Me.ComboBoxNomJoueur.TabIndex = 2
        '
        'BtnInfo
        '
        Me.BtnInfo.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnInfo.Font = New System.Drawing.Font("Moyko", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnInfo.Location = New System.Drawing.Point(441, 212)
        Me.BtnInfo.Name = "BtnInfo"
        Me.BtnInfo.Size = New System.Drawing.Size(312, 56)
        Me.BtnInfo.TabIndex = 3
        Me.BtnInfo.Text = "Details"
        Me.BtnInfo.UseVisualStyleBackColor = False
        '
        'BtnRetour
        '
        Me.BtnRetour.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnRetour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRetour.Font = New System.Drawing.Font("Moyko", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRetour.Location = New System.Drawing.Point(441, 655)
        Me.BtnRetour.Name = "BtnRetour"
        Me.BtnRetour.Size = New System.Drawing.Size(312, 55)
        Me.BtnRetour.TabIndex = 4
        Me.BtnRetour.Text = "Retour"
        Me.BtnRetour.UseVisualStyleBackColor = False
        '
        'BtnAlpha
        '
        Me.BtnAlpha.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnAlpha.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAlpha.Font = New System.Drawing.Font("Moyko", 9.749999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAlpha.Location = New System.Drawing.Point(12, 168)
        Me.BtnAlpha.Name = "BtnAlpha"
        Me.BtnAlpha.Size = New System.Drawing.Size(166, 26)
        Me.BtnAlpha.TabIndex = 5
        Me.BtnAlpha.Text = "Tri Alphabétique"
        Me.BtnAlpha.UseVisualStyleBackColor = False
        '
        'BtnTps
        '
        Me.BtnTps.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnTps.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnTps.Font = New System.Drawing.Font("Moyko", 9.749999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnTps.Location = New System.Drawing.Point(1014, 168)
        Me.BtnTps.Name = "BtnTps"
        Me.BtnTps.Size = New System.Drawing.Size(166, 26)
        Me.BtnTps.TabIndex = 6
        Me.BtnTps.Text = "Tri Par Temps"
        Me.BtnTps.UseVisualStyleBackColor = False
        '
        'LabelTitre
        '
        Me.LabelTitre.AutoSize = True
        Me.LabelTitre.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.LabelTitre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LabelTitre.Font = New System.Drawing.Font("Moyko", 60.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTitre.Location = New System.Drawing.Point(391, 9)
        Me.LabelTitre.Name = "LabelTitre"
        Me.LabelTitre.Size = New System.Drawing.Size(416, 96)
        Me.LabelTitre.TabIndex = 7
        Me.LabelTitre.Text = "Statistiques"
        '
        'Statistique
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1197, 748)
        Me.Controls.Add(Me.LabelTitre)
        Me.Controls.Add(Me.BtnTps)
        Me.Controls.Add(Me.BtnAlpha)
        Me.Controls.Add(Me.BtnRetour)
        Me.Controls.Add(Me.BtnInfo)
        Me.Controls.Add(Me.ComboBoxNomJoueur)
        Me.Controls.Add(Me.ListBoxTemps)
        Me.Controls.Add(Me.ListBoxJoueurs)
        Me.Name = "Statistique"
        Me.Text = "Statistique"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ListBoxJoueurs As ListBox
    Friend WithEvents ListBoxTemps As ListBox
    Friend WithEvents ComboBoxNomJoueur As ComboBox
    Friend WithEvents BtnInfo As Button
    Friend WithEvents BtnRetour As Button
    Friend WithEvents BtnAlpha As Button
    Friend WithEvents BtnTps As Button
    Friend WithEvents LabelTitre As Label
End Class
